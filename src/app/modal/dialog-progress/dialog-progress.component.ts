import {Component} from '@angular/core';
import {MySessionService} from '../../shared/my-session.service';

@Component({
    selector   : 'app-dialog-progress',
    templateUrl: './dialog-progress.component.html',
    styleUrls  : ['./dialog-progress.component.sass']
})
export class DialogProgressComponent {

    constructor(public mySession: MySessionService) {
    }
}
