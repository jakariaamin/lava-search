import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
    selector   : 'app-dialog-map',
    templateUrl: './dialog-map.component.html',
    styleUrls  : ['./dialog-map.component.scss']
})
export class DialogMapComponent implements AfterViewInit {
    @ViewChild('mapContainer', {static: false}) gmap: ElementRef;
    map: google.maps.Map;

    constructor(public dialogRef: MatDialogRef<DialogMapComponent>, @Inject(MAT_DIALOG_DATA) public params: any) {
    }

    ngAfterViewInit() {
        console.log('DialogMapComponent', this.params);
        const type = this.params.type;
        const lat  = this.params.lat;
        const lng  = this.params.lng;
        this.loadGoogleMap(lat, lng);
    }

    // Initialize Google Map with Coordinates
    loadGoogleMap(lat, lng) {
        const coordinates                        = new google.maps.LatLng(lat, lng);
        const mapOptions: google.maps.MapOptions = {
            center: coordinates,
            zoom  : 15,
        };
        this.map                                 = new google.maps.Map(this.gmap.nativeElement, mapOptions);
        // Call Map Marker Function
        this.dropMapMarker(coordinates);
    }

    // Draw Google Map Marker
    dropMapMarker(coordinates) {
        const marker = new google.maps.Marker({
            position: coordinates,
            map     : this.map,
        });
    }

    // Close Modal
    closeDialog() {
        this.dialogRef.close({});
    }
}
