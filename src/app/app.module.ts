import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

import {DialogProgressComponent} from './modal/dialog-progress/dialog-progress.component';
import {DialogAlertComponent} from './modal/dialog-alert/dialog-alert.component';
import {DialogMapComponent} from './modal/dialog-map/dialog-map.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {HomeComponent} from './home/home.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {AutocompleteComponent} from './shared/autocomplete.service';
import {AutocompleteHomeComponent} from './shared/autocompletehome.service';

const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {
        path      : '',
        redirectTo: '/home',
        pathMatch : 'full'
    },
    {
        path     : '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    declarations   : [
        AppComponent,
        HomeComponent,
        HeaderComponent,
        FooterComponent,
        PageNotFoundComponent,
        DialogProgressComponent,
        DialogAlertComponent,
        DialogMapComponent,
        AutocompleteComponent,
        AutocompleteHomeComponent,
    ],
    entryComponents: [
        DialogProgressComponent,
        DialogAlertComponent,
        DialogMapComponent,
    ],
    imports        : [
        NgbModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(
            appRoutes,
            {
                // enableTracing            : true,
                // anchorScrolling          : 'enabled',
                scrollPositionRestoration: 'top'
            }
        ),
        HttpClientModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers      : [],
    bootstrap      : [AppComponent]
})
export class AppModule {
}
