import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class MyConstants {
    public static APP_NAME          = 'Lava Search';
    public static APP_VERSION       = '1.0.0';
    public static APP_BUILD_VERSION = 1;
    public static API_VERSION       = '1.0.0';
    public static APP_PLATFORM      = 'Angular';
    public static APP_COPYRIGHT     = '';

    public static HTTP_GET    = 'GET';
    public static HTTP_POST   = 'POST';
    public static HTTP_PUT    = 'PUT';
    public static HTTP_DELETE = 'DELETE';

    public static TOKEN_KEY    = 'auth-token';
    public static PASSWORD_KEY = 'password';

    public static GOOGLE_MAP_API_KEY = 'AIzaSyACO8TRldIn1TkQQ27KP6elI6KCE_3gNHM';

    public static NATSTORG_SETTINGS = 'settings';

    public static API_DOMAIN = '';

    public static API_FILE_EXTENSION = '';
    public static API_TEXT           = 'api/v1/';
    public static API_AUTH           = 'auth/';

    public static API_LOGIN: string    = MyConstants.API_DOMAIN + MyConstants.API_TEXT + 'login' + MyConstants.API_FILE_EXTENSION;
    public static API_REGISTER: string = MyConstants.API_DOMAIN + MyConstants.API_TEXT + 'register' + MyConstants.API_FILE_EXTENSION;
}

