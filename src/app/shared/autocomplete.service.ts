import {Component, ViewChild, EventEmitter, Output, OnInit, AfterViewInit, Input} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
    selector: 'AutocompleteComponent',
    template: `
        <input type = "text" class = "form-control" [(ngModel)] = "autocompleteInput" #addresstext>
    `,
})
export class AutocompleteComponent implements OnInit, AfterViewInit {
    @Input() adressType: string;
    @Input() centerLat: number;
    @Input() centerLng: number;
    @Output() setAddress: EventEmitter<any> = new EventEmitter();
    @ViewChild('addresstext', {static: false}) addresstext: any;

    autocompleteInput: string;
    queryWait: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.getPlaceAutocomplete();
    }

    private getPlaceAutocomplete() {

        const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
            {
                componentRestrictions: {country: 'MY'},
                types                : ['establishment'],  // 'establishment' / 'address' / 'geocode'
            });

        const circle = new google.maps.Circle({
            center: new google.maps.LatLng(this.centerLat, this.centerLng),
            radius: 40000  // in meter, 40000 = 40km
        });
        autocomplete.setBounds(circle.getBounds()); // Set option to only search within radius
        autocomplete.setOptions({strictBounds: true}); // Force Set Search Area Restriction

        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const place = autocomplete.getPlace();
            this.invokeEvent(place);
        });
    }

    invokeEvent(place: object) {
        this.setAddress.emit(place);
    }
}
